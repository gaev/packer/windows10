# Packer | Windows

> Création et configuration d'une Machine Virtuelle VirtualBox avec l'outil __Packer__.

Ce dépôt s'inspire du projet [packer-windows](https://github.com/StefanScherer/packer-windows)

[[_TOC_]]

## Dépendances

* [Packer](https://www.packer.io/downloads)
* [VirtalBox](https://www.virtualbox.org/wiki/Downloads) et extension Pack
* Une [iso](https://www.microsoft.com/fr-fr/software-download/windows10ISO) de Windows 10

## Packer et Template json

__Packer__ nous permet de créer des machines identiques depuis une ISO pour de multiple platforms depuis un simple fichier (_Template_).  
Le fichier _Template_ est une fichier _json_ qui décrit comment créer la machine virtuelle.  cf : [packer\_win10.json](packer_win10.json).  

Les _Builders_ (Providers) sont réponsable de construire l'image de la machine pour une plateforme specifique (VMWare, VirutalBox, Azure, AWS...).  
Les _Provisioners_ installent et configurent une machine. __Packer__ permet du provisioning simple (copies fichier,script, install package), il est préférable d'utiliser un autre outils de provisioning type Vagrant, Ansible.  
Les Communications se fait principalement via SSH et WinRM (pour Windows).  

## Installation Windows

Pour créer une VirtualBox avec l'OS Windows, il faut disposer d'une ISO de la version de Windows voulu ainsi que les fichiers de ce dépôt.  
Le fichier le plus important, qui permet d'installer Windows en mode automatique sans interation, et le fichier xml [autounattend.xml](answer_file/autounattend.xml).  

### autounattend.xml

Permet l'installation de Windows sans interraction.  

Documentation Windows pour créer le fichier:  
* [answer file](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/update-windows-settings-and-scripts-create-your-own-answer-file-sxs)
* [Les components](https://docs.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/components-b-unattend)

il faut créer le fichier depuis Windows, oui c'est reloux, sinon le trouver sur un dépôt et le modifier. Comme ici [answer\_file/autounattend.xml](answer_file/autounattend.xml).  

Dans notre version: Windows est parametré en français et sans licence (version d'évaluation de 90 jours Windows 10 Pro) avec l'utilisateur _vagrant_ et mot de passe _vagrant_.  
Entre la balise _\<FirstLogonCommand\>_ plusieurs commandes/scripts sont exécutés pour configurer Windows telque la mise à jour, l'activation de WinRM, la configuration réseau... Certains scripts sont disponible ici [scripts](scripts/), ils sont montés par packer dans la MV.

### Les scripts

les [scripts](scripts) ont des noms explicites.  
[scripts/enable-guest-additions.ps1](scripts/enable-guest-additions.ps1) permet d'installer et d'activer les fonctionnalitées VirtualBox. (_Packer_ va automatiquement upload le fichier _VBoxGuestAdditions.iso_ dans la MV.)

## Build

Pour lancer __Packer__ il suffit de lancer la commande ```packer build <Template.json>```  

Voici la commande utilisé pour ce dépôt :  
```
packer build --only=virtualbox-iso \
  --var iso_url=./19041.264.200511-0456.vb_release_svc_refresh_CLIENTENTERPRISEEVAL_OEMRET_x64FRE_fr-fr.iso \
  --var iso_checksum=2965FD214FE77D3C80BADCF0B907399DA6636675422C64AE3C0D5DABB2B90C66 \
  --var autounattend=./answer_files/autounattend.xml \
  --var cloud-token=VagrantToken \
  packer_win10.json
```

Cela va créer un fichier __packer\_virtualbox\_windows10.box__.

## Post Build | Vagrant Box

__Packer__ permet de publier notre MV sur __VagrantCloud__.  Sans rentrer dans les détails, __Packer__ va créer une box (format __Vagrant__) qui n'est autre qu'un tar de notre MV avec le fichier de parametrisation.  
Par la suite, notre MV est accessible depuis __VagrantCloud__ en public et utilisable par l'outil __Vagrant__.

* Le dépôt de notre box Windows 10 Pro : [GAEV/win10](https://app.vagrantup.com/GAEV/boxes/win10)
* Le dépôt du projet : [https://app.vagrantup.com/GAEV](https://app.vagrantup.com/GAEV)

;-)
